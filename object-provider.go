package di

import (
	"errors"
	"reflect"
)

type Constructor = func() (interface{}, error)

type mapConstructor struct {
	Constructor Constructor
	Once        bool
}

type injectionProperty struct {
	Pos    int
	Name   string
	Object string
}

type ObjectProvider struct {
	objects        map[string]mapConstructor
	onceObjects    map[string]interface{}
	typeInjections map[string][]*injectionProperty
}

var objectProvider = &ObjectProvider{
	objects:        make(map[string]mapConstructor),
	onceObjects:    make(map[string]interface{}),
	typeInjections: make(map[string][]*injectionProperty),
}

func (p *ObjectProvider) Add(object string, createOnce bool, constructor Constructor) {
	p.objects[object] = mapConstructor{
		Constructor: constructor,
		Once:        createOnce,
	}
}

func (p *ObjectProvider) Create(object string) (interface{}, error) {
	if info, ok := p.objects[object]; !ok {
		return nil, errors.New("object \"" + object + "\" not found")
	} else {
		if info.Once {
			if o, ok := p.onceObjects[object]; ok {
				return o, nil
			}
		}
		o, err := info.Constructor()
		if err != nil {
			return nil, err
		}
		err = p.init(o)
		if err != nil {
			return nil, err
		}
		if info.Once {
			p.onceObjects[object] = o
		}
		return o, nil
	}
}

func (p *ObjectProvider) init(obj interface{}) error {
	t := reflect.TypeOf(obj)
	injections := p.getInjections(t)
	value := reflect.ValueOf(obj).Elem()
	for i := range injections {
		injection := injections[i]
		f := value.FieldByName(injection.Name)
		injectionObj, err := p.Create(injection.Object)
		if err != nil {
			return err
		}
		k := f.Kind()
		switch k {
		case reflect.Interface:
			f.Set(reflect.ValueOf(injectionObj))
		case reflect.Pointer:
			f.Set(reflect.ValueOf(injectionObj))
		default:
			f.Set(reflect.ValueOf(injectionObj).Elem())
		}
	}

	return nil
}

func (p *ObjectProvider) getInjections(value reflect.Type) []*injectionProperty {
	if value.Kind() == reflect.Ptr {
		value = value.Elem()
	}
	key := value.String()
	if res, ok := p.typeInjections[key]; ok {
		return res
	}
	var res []*injectionProperty
	for i := 0; i < value.NumField(); i++ {
		f := value.Field(i)
		tag := f.Tag.Get("inject")
		if tag == "" {
			continue
		}
		res = append(res, &injectionProperty{
			Pos:    i,
			Name:   f.Name,
			Object: tag,
		})
	}
	p.typeInjections[key] = res
	return res
}

func Add(object string, createOnce bool, constructor Constructor) {
	objectProvider.Add(object, createOnce, constructor)
}

func Create[T interface{}](object string) (T, error) {
	var zero T
	o, err := objectProvider.Create(object)
	if err != nil {
		return zero, err
	}
	return o.(T), nil
}
