# Dependency Injection

## Install 

```bash
go get gitlab.com/shadowy/go/dependency-injection
```

## Using

### Object definition

```go
package example

type TestStruct1 struct {
	ID string
}

func (s *TestStruct1) GetID() string {
	return s.ID
}

func CreateTestStruct1() (interface{}, error) {
	return new(TestStruct1), nil
}

type TestStruct2 struct {
	ID string
}

func (s *TestStruct2) GetID() string {
	return s.ID
}

func CreateTestStruct2() (interface{}, error) {
	return new(TestStruct2), nil
}

type TestInterface interface {
	GetID() string
}

type TestStruct3 struct {
	Struct1 *TestStruct1 `inject:"test-01"`
	Struct2 TestStruct2  `inject:"test-02"`
}

func CreateTestStruct3() (interface{}, error) {
	return new(TestStruct3), nil
}

```
### Add object to injector

```go
package example

import "gitlab.com/shadowy/go/dependency-injection"

func AddToInjector {
	di.Add("test-01", true, CreateTestStruct1)
	di.Add("test-02", false, CreateTestStruct2)
	di.Add("test-03", false, CreateTestStruct3)
}

```

### Object creation

```go
package example

import "gitlab.com/shadowy/go/dependency-injection"

func Create() {
	o, err :=di.Create[*TestStruct3]("test-03")
}
```
