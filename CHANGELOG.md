# CHANGELOG

<!--- next entry here -->

## 0.1.2
2023-01-03

### Fixes

- fix issue (a5559411422d3db366f7dae750732c01f588383e)

## 0.1.1
2023-01-03

### Fixes

- fix working with interface (8fe736cc68b6430c67fe56a8c41a3f04f8162e72)
- fix tests (e3bcf8be7cb64ed4fdd634f632818f438efcdfbf)

## 0.1.0
2022-12-31

### Features

- initial commit (85ab3c43b50bbcaa06e9060160a2fcf4fe172be7)

