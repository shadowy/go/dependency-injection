package di

import (
	"testing"
)

type TestStruct1 struct {
	ID string
}

func (s *TestStruct1) GetID() string {
	return s.ID
}

func CreateTestStruct1() (interface{}, error) {
	return new(TestStruct1), nil
}

type TestStruct2 struct {
	ID string
}

func (s *TestStruct2) GetID() string {
	return s.ID
}

func CreateTestStruct2() (interface{}, error) {
	return new(TestStruct2), nil
}

type TestInterface interface {
	GetID() string
}

type TestStruct3 struct {
	Struct1 *TestStruct1 `inject:"test-01"`
	Struct2 TestStruct2  `inject:"test-02"`
}

func CreateTestStruct3() (interface{}, error) {
	return new(TestStruct3), nil
}

type TestStruct4 struct {
	Struct1 *TestStruct1 `inject:"test-not-exist"`
}

func CreateTestStruct4() (interface{}, error) {
	return new(TestStruct4), nil
}

type TestStruct5 struct {
	Struct1 TestInterface `inject:"test-02"`
}

func CreateTestStruct5() (interface{}, error) {
	return new(TestStruct5), nil
}

func init() {
	Add("test-01", true, CreateTestStruct1)
	Add("test-02", false, CreateTestStruct2)
	Add("test-03", false, CreateTestStruct3)
	Add("test-04", false, CreateTestStruct4)
	Add("test-05", false, CreateTestStruct5)
}
func TestCreate1(t *testing.T) {
	o, err := Create[*TestStruct1]("test-01")
	if err != nil {
		t.Error(err)
	}
	if o.ID != "" {
		t.Error("ID not empty")
	}
	o.ID = "1"
	o1, err1 := Create[*TestStruct1]("test-01")
	if err1 != nil {
		t.Error(err)
	}
	if o1 != o {
		t.Error("Object not equal")
	}
}

func TestCreate2(t *testing.T) {
	o, err := Create[*TestStruct2]("test-02")
	if err != nil {
		t.Error(err)
	}
	if o.ID != "" {
		t.Error("ID not empty")
	}
	o.ID = "1"
	o1, err1 := Create[*TestStruct2]("test-02")
	if err1 != nil {
		t.Error(err)
	}
	if o1 == o {
		t.Error("Object equal")
	}
}

func TestCreate3(t *testing.T) {
	o, err := Create[*TestStruct3]("test-03")
	if err != nil {
		t.Error(err)
	}
	if o.Struct1 == nil {
		t.Error("Struct1 is nil")
		return
	}
	if o.Struct1.ID == "" {
		t.Error("ID empty")
		return
	}
	if o.Struct2.ID != "" {
		t.Error("ID not empty")
		return
	}
}

func TestCreate4(t *testing.T) {
	o, err := Create[*TestStruct4]("test-04")
	if err == nil {
		t.Error("Should generate error")
	}
	if o != nil {
		t.Error("Object should be nil")
	}
}

func TestCreate5(t *testing.T) {
	o, err := Create[*TestStruct5]("test-05")
	if err != nil {
		t.Error("Should generate error")
	}
	if o == nil {
		t.Error("Object should be not nil")
	}
}
